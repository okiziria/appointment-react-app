import logo from './logo.svg';
import './App.css';
import Calendar from './components/calendar';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Unstable_Grid2';
import CreateApp from './components/createapp';
import Appointments from './components/appointments';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


function App() {
  return (
    <div className="App">

      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid xs={4}>
            <CreateApp />
            <Item>
              <Calendar />
            </Item>
          </Grid>
          <Grid xs={8}>
            <Item>
              <Appointments/>
            </Item>
          </Grid>
        </Grid>
      </Box>

    </div>
  );
}

export default App;
