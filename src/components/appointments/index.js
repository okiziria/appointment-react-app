import { React, useContext, useState } from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import FolderIcon from '@mui/icons-material/Folder';
import DeleteIcon from '@mui/icons-material/Delete';

import CalendarContext from '../../context/calendar/';
import dayjs from 'dayjs';

import Popup from './popup';


const Demo = styled('div')(({ theme }) => ({
    backgroundColor: theme.palette.background.paper,
}));

function Appointments() {
    const { apps } = useContext(CalendarContext);
    const { UpdateApps } = useContext(CalendarContext);

    const [dense, setDense] = useState(true);
    const [secondary, setSecondary] = useState(true);

    const [open, setOpen] = useState(0);

    const [utitle, setUtitle] = useState();
    const [utime, setUtime] = useState();
    const [udate, setUdate] = useState();
    const [uid, setUid] = useState(0);



    const editApp = (id) => {
        setOpen(open + 1);
        setUtime(apps[id].time);
        setUdate(apps[id].date);
        setUtitle(apps[id].title);
        setUid(id);
        console.log(utitle, "id");

    }

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={12} md={12}>
                    <Demo>
                        <List dense={dense}>
                            {apps.map((value, index) =>
                                <ListItem key={index}
                                    secondaryAction={
                                        <IconButton edge="end" aria-label="delete">
                                            <DeleteIcon />
                                        </IconButton>
                                    }
                                >
                                    <ListItemAvatar>
                                        <Avatar>
                                            <FolderIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={<b>{value.title}</b>}
                                        secondary={secondary ? <b> {value.date + " " + dayjs(value.time).format('hh') + ":" + dayjs(value.time).format('mm')}  </b> : null}
                                        onClick={() => editApp(index)}
                                    />

                                </ListItem>,


                            )}
                        </List>
                    </Demo>
                </Grid>
            </Grid>

            {open > 0 && <Popup isOpen={open} udate={udate} utitle={utitle} utime={utime} uid={uid} />}
        </div>
    );
}

export default Appointments;