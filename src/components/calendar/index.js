import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from "react-router-dom";

import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker/StaticDatePicker'

import CalendarContext from '../../context/calendar/';





export default function Calendar() {
  const { date } = useContext(CalendarContext);
  const { UpdateCalendar } = useContext(CalendarContext);
  const navigate = useNavigate();

  const [value, setValue] = useState(dayjs(Date('YYYY-MM-DD')));

  useEffect(() => {
    navigate("/" + value.$D + "/" + value.$M + "/" + value.$y);
    UpdateCalendar(value.$D, value.$M, value.$y);
    
  }, [value]);
  


  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <StaticDatePicker
          orientation="landscape"
          showToolbar={false}
          open={false}
          openTo="day"
          value={value}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider>
    </div>
  );
}

