import React from "react";
import { useEffect, useState, useContext } from "react";

import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Icon from '@mui/material/Icon';

import CalendarContext from '../../context/calendar/';
import Popup from "./popup";

function CreateApp(){
    const { date } = useContext(CalendarContext);
    const { apps } = useContext(CalendarContext);

    const [open, setOpen] = useState(0);

    const create = ()=>{
        setOpen(open + 1);
    }

    return(
        <div>
        
           
            <Button variant="outlined" onClick={create}>Create <Icon color="primary">add_circle</Icon></Button>
            <Popup isOpen={open} date={date} />
        </div>
    );
}

export default CreateApp;