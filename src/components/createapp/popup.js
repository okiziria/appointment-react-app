import { React, useEffect, useState, useContext } from "react";

import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';


import dayjs from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { MobileTimePicker } from '@mui/x-date-pickers/MobileTimePicker';
import { DesktopTimePicker } from '@mui/x-date-pickers/DesktopTimePicker';
import { StaticTimePicker } from '@mui/x-date-pickers/StaticTimePicker';

import CalendarContext from '../../context/calendar/';


function Popup({ isOpen, date }) {
    const { CreateApps } = useContext(CalendarContext);
    const { apps } = useContext(CalendarContext);

    const [open, setOpen] = useState(false);
    const [title, setTitle] = useState("");

    const [timevalue, setTimevalue] = useState();

    useEffect(() => {
        if (isOpen > 0) {
            setOpen(true);
        }
    }, [isOpen]);

    const handleChange = (e) => {
        setTitle(e.target.value);
    }

    const handleCreate = () => {
        CreateApps(title, date, timevalue);
        setOpen(false);
    }

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle>Appointment</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    To make appointment fill title form
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    value={title}
                    onChange={handleChange}
                    label="Title"
                    type="text"
                    fullWidth
                    variant="standard"
                />

                <DialogContentText>Choose time</DialogContentText>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
         
          <MobileTimePicker defaultValue={dayjs('2022-04-17T15:30')} 
          renderInput={(params) => <TextField {...params} />}
          value={timevalue}
          onChange={(newValue) => {
            setTimevalue(newValue);
          }}
          />
    </LocalizationProvider>



            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleCreate}>Create</Button>
            </DialogActions>
        </Dialog>
    )
}

export default Popup;