import { createContext, useState } from "react";



const CalendarContext = createContext();

export function CalendarProvider({ children }) {

    const [date, setDate] = useState(Date('YYYY-MM-DD'))

    const UpdateCalendar = (day, month, year) => {
        setDate(day + "-" + month + "-" + year);
    };

    const [apps, setApps] = useState([]);

    const CreateApps = (title, date, time) => {
        setApps((prevState)=>[...prevState, {title, date, time}]);
    };

    const UpdateApps = (id, newtitle, newdate, newtime) => { 
         
        setApps((prevState)=>[...prevState.slice(0, id), {title: newtitle, date: newdate, time: newtime}, ...prevState.slice(id+1)]);
        console.log(id, "apps"); 
    }

    return (
        <CalendarContext.Provider
            value={{ date, UpdateCalendar, apps, CreateApps, UpdateApps }}>
            {children}
        </CalendarContext.Provider>
    )
}

export default CalendarContext;