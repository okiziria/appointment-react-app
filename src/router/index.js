import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from "../App";
import { CalendarProvider } from '../context/calendar';


function PageRouter() {
    return (
        <CalendarProvider>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<App />} />
                    <Route path='/:day/:month/:year' element={<App />} />
                </Routes>
            </BrowserRouter>
        </CalendarProvider>
    )
}

export default PageRouter;